﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Authentication
{
    public class JwtConfig
    {
        public string Secret { get; set; }
    }
}
