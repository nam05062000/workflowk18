﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Interface
{
    public interface IUnitOfWork
    {
        IUserRepository Users { get; }
        IProjectRepository Projects { get; }
        IInviteRepository Invites { get; }
        ICardRepository Cards { get; }
        ITasksRepository Tasks { get; }
    }
}
