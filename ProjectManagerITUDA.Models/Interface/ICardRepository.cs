﻿using ProjectManagerITUDA.Models.Dto;
using ProjectManagerITUDA.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ProjectManagerITUDA.Models.Interface
{
    public interface ICardRepository
    {
        Task<Card> GetByIdAsync(Guid id);
        Task<IEnumerable<Card>> GetAllAsync();
        Task<Card> AddAsync(CreateCardDto entity);
        Task<Card> UpdateAsync(UpdateCardDto entity, Guid id);
        Task DeleteAsync(Guid id);
    }
}
