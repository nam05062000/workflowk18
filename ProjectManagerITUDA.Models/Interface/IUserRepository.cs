﻿using ProjectManagerITUDA.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Interface
{
    public interface IUserRepository
    {
        Task<User> checkLogin(string username, string password);
        Task<User> getProfile(Guid id);
        Task<User> checkRegister(string username);
        Task<User> registerUser(string username, string password, string firstname, string lastname);
        
        //Task<User> GetByIdAsync(Guid id);
    }
}
