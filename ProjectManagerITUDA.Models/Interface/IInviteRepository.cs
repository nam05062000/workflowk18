using ProjectManagerITUDA.Models.Dto;
using ProjectManagerITUDA.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Interface
{
    public interface IInviteRepository
    {
        Task<IEnumerable<Invite>> GetAllAsync();
        Task<Invite> AddAsync(CreateInviteDto entity, Guid createdBy);
        Task<Invite> UpdateAsync(UpdateInviteDto entity, Guid id);
        System.Threading.Tasks.Task DeleteAsync(Guid id);
        Task<Invite> GetByIdAsync(Guid id);
    }
}
