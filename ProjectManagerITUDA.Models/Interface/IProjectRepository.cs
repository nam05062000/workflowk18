﻿using ProjectManagerITUDA.Models.Dto;
using ProjectManagerITUDA.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Interface
{
    public interface IProjectRepository
    {
        Task<IEnumerable<Project>> GetAllAsync();
        Task<Project> AddAsync(CreateProjectDto entity, Guid createdBy);
        Task<Project> UpdateAsync(UpdateProjectDto entity, Guid id);
        System.Threading.Tasks.Task DeleteAsync(Guid id);
        Task<Project> GetByIdAsync(Guid id);
    }
}
