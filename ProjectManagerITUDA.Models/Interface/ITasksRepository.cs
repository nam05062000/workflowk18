﻿using ProjectManagerITUDA.Models.Dto;
using ProjectManagerITUDA.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Interface
{
    public interface ITasksRepository
    {
        Task<IEnumerable<Tasks>> GetAllAsync();
        Task<Tasks> AddAsync(CreateTasksDto entity,Guid guid);
        Task<Tasks> UpdateAsync(UpdateTasksDto entity, Guid id);
        Task DeleteAsync(Guid id);
        Task<Tasks> GetByIdAsync(Guid id);
    }
}
