﻿using System;
using System.Collections.Generic;

namespace ProjectManagerITUDA.Models.Entities
{
    public partial class Project
    {
        public Project()
        {
            CardUserMembers = new HashSet<CardUserMember>();
            Cards = new HashSet<Card>();
            Invites = new HashSet<Invite>();
            MemberProjects = new HashSet<MemberProject>();
            TicketProjects = new HashSet<TicketProject>();
            TimesheetProjects = new HashSet<TimesheetProject>();
        }

        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public string? Name { get; set; }
        public int? NumberMember { get; set; }
        public string? Icon { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? TimeExpiry { get; set; }
        public bool? IsActive { get; set; }

        public virtual User? CreatedByNavigation { get; set; }
        public virtual ICollection<CardUserMember> CardUserMembers { get; set; }
        public virtual ICollection<Card> Cards { get; set; }
        public virtual ICollection<Invite> Invites { get; set; }
        public virtual ICollection<MemberProject> MemberProjects { get; set; }
        public virtual ICollection<TicketProject> TicketProjects { get; set; }
        public virtual ICollection<TimesheetProject> TimesheetProjects { get; set; }
    }
}
