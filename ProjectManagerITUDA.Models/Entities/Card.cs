﻿using System;
using System.Collections.Generic;

namespace ProjectManagerITUDA.Models.Entities
{
    public partial class Card
    {
        public Card()
        {
            CardUserMembers = new HashSet<CardUserMember>();
            Tasks = new HashSet<Tasks>();
        }

        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ProjectId { get; set; }
        public string? Name { get; set; }
        public int? NumberMember { get; set; }
        public string? Icon { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? TimeExpiry { get; set; }
        public bool? IsActive { get; set; }

        public virtual User? CreatedByNavigation { get; set; }
        public virtual Project? Project { get; set; }
        public virtual ICollection<CardUserMember> CardUserMembers { get; set; }
        public virtual ICollection<Tasks> Tasks { get; set; }
    }
}
