﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Dto
{
    public class CreateCardDto
    {
        public Guid? CreatedBy { get; set; }
        public Guid? ProjectId { get; set; }
        public string? Name { get; set; }
        public int? NumberMember { get; set; }
        public string? Icon { get; set; }
        public DateTime? TimeExpiry { get; set; }
        public bool? IsActive { get; set; }
    }
}
