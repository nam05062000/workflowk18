﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Dto
{
    public class CreateTasksDto
    {
        public Guid? CreatedBy { get; set; }
        public Guid? CardId { get; set; }
        public string? Name { get; set; }
        public int? NumberMember { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? TimeExpiry { get; set; }
        public string? Comment { get; set; }
        public string? Icon { get; set; }
        public string? Type { get; set; }
        public bool? IsActive { get; set; }
    }
}
