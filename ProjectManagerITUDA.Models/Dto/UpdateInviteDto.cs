using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Dto
{
    public class UpdateInviteDto
    {
        public string? Content { get; set; }
        public DateTime? TimeExpiry { get; set; }
        public bool? IsActive { get; set; }
    }
}
