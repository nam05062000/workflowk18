﻿using ProjectManagerITUDA.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(IUserRepository userRepository, IProjectRepository projectRepository)
        {

            Users = userRepository;
            Projects = projectRepository;
        }
        public IUserRepository Users { get; }
        public IProjectRepository Projects { get; }

        public IInviteRepository Invites => throw new NotImplementedException();

        public ICardRepository Cards => throw new NotImplementedException();

        public ITasksRepository Tasks => throw new NotImplementedException();
    }
}
