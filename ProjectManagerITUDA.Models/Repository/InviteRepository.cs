using Dapper;
using Microsoft.Extensions.Configuration;
using ProjectManagerITUDA.Models.Dto;
using ProjectManagerITUDA.Models.Entities;
using ProjectManagerITUDA.Models.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Repository
{
    public class InviteRepository : IInviteRepository
    {
        private readonly IConfiguration configuration;
        private object createBy;
        private object ProjectId;

        public InviteRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task<Invite> AddAsync(CreateInviteDto entity, Guid createdBy)
        {
            var procedureName = "spCreateInvites";
            var parameters = new DynamicParameters();
            parameters.Add("CreatedBy", createBy, DbType.Guid, ParameterDirection.Input);
            parameters.Add("ProjectId", ProjectId, DbType.Guid, ParameterDirection.Input);
            parameters.Add("Content", entity.Content, DbType.String, ParameterDirection.Input);
            parameters.Add("TimeExpiry", entity.TimeExpiry, DbType.DateTime, ParameterDirection.Input);
            parameters.Add("IsActive", entity.IsActive, DbType.Boolean, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryFirstOrDefaultAsync<Project>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
                var invite = new Invite()
                {
                    Content = entity.Content,
                    CreatedBy = createdBy,
                    TimeExpiry = entity.TimeExpiry,
                    IsActive = entity.IsActive,
                };
                return invite;
            }
        }

        public async System.Threading.Tasks.Task DeleteAsync(Guid id)
        {
            var procedureName = "spDeleteInvites";
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Guid, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                await connection.QueryFirstOrDefaultAsync<Invite>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Invite>> GetAllAsync()
        {
            var procedureName = "EXECUTE spGetAllInvites";

            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryAsync<Invite>(procedureName);
                return result.ToList();
            }
        }

        public async Task<Invite> GetByIdAsync(Guid id)
        {
            var procedureName = "spGetByIdInvites";
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Guid, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryFirstOrDefaultAsync<Invite>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }

        public async Task<Invite> UpdateAsync(UpdateInviteDto entity, Guid id)
        {
            var procedureName = "spCreateInvites";
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Guid, ParameterDirection.Input);
            parameters.Add("Content", entity.Content, DbType.String, ParameterDirection.Input);
            parameters.Add("TimeExpiry", entity.TimeExpiry, DbType.DateTime, ParameterDirection.Input);
            parameters.Add("IsActive", entity.IsActive, DbType.Boolean, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryFirstOrDefaultAsync<Project>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
                var invite = new Invite()
                {
                    Id = id,
                    Content = entity.Content,
                    TimeExpiry = entity.TimeExpiry,
                    IsActive = entity.IsActive,
                };
                return invite;
            }
        }
    }
}
