﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProjectManagerITUDA.Models.Dto;
using ProjectManagerITUDA.Models.Entities;
using ProjectManagerITUDA.Models.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Repository
{
    public class CardRepository : ICardRepository
    {
        private readonly IConfiguration configuration;
        public CardRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task<Card> AddAsync(CreateCardDto entity)
        {
            var procedureName = "spCreateCards";
            var parameters = new DynamicParameters();
            parameters.Add("CreatedBy", entity.CreatedBy, DbType.Guid, ParameterDirection.Input);
            parameters.Add("Name", entity.Name, DbType.String, ParameterDirection.Input);
            parameters.Add("NumberMember", entity.NumberMember, DbType.Int32, ParameterDirection.Input);
            parameters.Add("Icon", entity.Icon, DbType.String, ParameterDirection.Input);
            parameters.Add("TimeExpiry", entity.TimeExpiry, DbType.DateTime, ParameterDirection.Input);
            parameters.Add("IsActive", entity.IsActive, DbType.Boolean, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryFirstOrDefaultAsync<Card>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
                var card = new Card()
                {
                    Name = entity.Name,
                    CreatedBy = entity.CreatedBy,
                    NumberMember = entity.NumberMember,
                    Icon = entity.Icon,
                    TimeExpiry = entity.TimeExpiry,
                    IsActive = entity.IsActive,
                };
                return card;
            }
        }

        public async System.Threading.Tasks.Task DeleteAsync(Guid id)
        {
            var procedureName = "spDeleteCards";
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Guid, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                await connection.QueryFirstOrDefaultAsync<Card>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Card>> GetAllAsync()
        {
            var procedureName = "EXECUTE spGetAllCards";

            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryAsync<Card>(procedureName);
                return result.ToList();
            }
        }

        public async Task<Card> GetByIdAsync(Guid id)
        {
            var procedureName = "spGetByIdCards";
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Guid, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryFirstOrDefaultAsync<Card>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }

        public async Task<Card> UpdateAsync(UpdateCardDto entity, Guid id)
        {
            var procedureName = "spUpdateCards";
            var parameters = new DynamicParameters();
            parameters.Add("CreatedBy", entity.CreatedBy, DbType.Guid, ParameterDirection.Input);
            parameters.Add("Name", entity.Name, DbType.String, ParameterDirection.Input);
            parameters.Add("NumberMember", entity.NumberMember, DbType.Int32, ParameterDirection.Input);
            parameters.Add("Icon", entity.Icon, DbType.String, ParameterDirection.Input);
            parameters.Add("TimeExpiry", entity.TimeExpiry, DbType.DateTime, ParameterDirection.Input);
            parameters.Add("IsActive", entity.IsActive, DbType.Boolean, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryFirstOrDefaultAsync<Card>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
                var card = new Card()
                {
                    Id = id,
                    Name = entity.Name,
                    NumberMember = entity.NumberMember,
                    Icon = entity.Icon,
                    TimeExpiry = entity.TimeExpiry,
                    IsActive = entity.IsActive,
                };
                return card;
            }
        }
    }
}
