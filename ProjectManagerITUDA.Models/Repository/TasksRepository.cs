﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProjectManagerITUDA.Models.Dto;
using ProjectManagerITUDA.Models.Entities;
using ProjectManagerITUDA.Models.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Repository
{
    public class TasksRepository : ITasksRepository
    {
        private readonly IConfiguration configuration;
        public TasksRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task<Tasks> AddAsync(CreateTasksDto entity,Guid guid)
        {
            var procedureName = "spCreateTasks";
            var parameters = new DynamicParameters();
            parameters.Add("CreatedBy", entity.CreatedBy, DbType.Guid, ParameterDirection.Input);
            parameters.Add("CardId", entity.CardId, DbType.Guid, ParameterDirection.Input);
            parameters.Add("Name", entity.Name, DbType.String, ParameterDirection.Input);
            parameters.Add("NumberMember", entity.NumberMember, DbType.Int32, ParameterDirection.Input);
            parameters.Add("Comment", entity.Comment, DbType.String, ParameterDirection.Input);
            parameters.Add("Icon", entity.Icon, DbType.String, ParameterDirection.Input);
            parameters.Add("Type", entity.Type, DbType.String, ParameterDirection.Input);
            parameters.Add("IsActive", entity.IsActive, DbType.Boolean, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryFirstOrDefaultAsync<Tasks>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
                var tasks = new Tasks()
                {
                    CreatedBy = entity.CreatedBy,
                    CardId = entity.CardId,
                    Name = entity.Name,
                    NumberMember = entity.NumberMember,
                    Comment = entity.Comment,   
                    Icon = entity.Icon,
                    Type = entity.Type,
                    IsActive = entity.IsActive,
                    
                };
                return tasks;
            }
        }

        public async Task DeleteAsync(Guid id)
        {
            var procedureName = "spDeleteTasks";
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Guid, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                await connection.QueryFirstOrDefaultAsync<Tasks>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Tasks>> GetAllAsync()
        {
            var procedureName = "EXECUTE spGetAllTasks";

            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryAsync<Tasks>(procedureName);
                return result.ToList();
            }
        }

        public async Task<Tasks> GetByIdAsync(Guid id)
        {
            var procedureName = "spGetByIdTasks";
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Guid, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryFirstOrDefaultAsync<Tasks>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }

        public async Task<Tasks> UpdateAsync(UpdateTasksDto entity, Guid id)
        {
            var procedureName = "spCreateTasks";
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Guid, ParameterDirection.Input);
            parameters.Add("CreatedBy", entity.CreatedBy, DbType.Guid, ParameterDirection.Input);
            parameters.Add("CardId", entity.CardId, DbType.Guid, ParameterDirection.Input);
            parameters.Add("Name", entity.Name, DbType.String, ParameterDirection.Input);
            parameters.Add("NumberMember", entity.NumberMember, DbType.Int32, ParameterDirection.Input);
            parameters.Add("Comment", entity.Comment, DbType.String, ParameterDirection.Input);
            parameters.Add("Icon", entity.Icon, DbType.String, ParameterDirection.Input);
            parameters.Add("Type", entity.Type, DbType.String, ParameterDirection.Input);
            parameters.Add("IsActive", entity.IsActive, DbType.Boolean, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryFirstOrDefaultAsync<Tasks>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
                var tasks = new Tasks()
                {
                    Id = id,
                    CreatedBy = entity.CreatedBy,
                    CardId = entity.CardId,
                    Name = entity.Name,
                    NumberMember = entity.NumberMember,
                    Comment = entity.Comment,
                    Icon = entity.Icon,
                    Type = entity.Type,
                    IsActive = entity.IsActive,

                };
                return tasks;
            }
        }
    }
}
