﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProjectManagerITUDA.Models.Dto;
using ProjectManagerITUDA.Models.Entities;
using ProjectManagerITUDA.Models.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerITUDA.Models.Repository
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly IConfiguration configuration;
        public ProjectRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task<Project> AddAsync(CreateProjectDto entity, Guid createBy)
        {
            var procedureName = "spCreateProject";
            var parameters = new DynamicParameters();
            parameters.Add("CreatedBy", createBy, DbType.Guid, ParameterDirection.Input);
            parameters.Add("Name", entity.Name, DbType.String, ParameterDirection.Input);
            parameters.Add("NumberMember", entity.NumberMember, DbType.Int32, ParameterDirection.Input);
            parameters.Add("Icon", entity.Icon, DbType.String, ParameterDirection.Input);
            parameters.Add("TimeExpiry", entity.TimeExpiry, DbType.DateTime, ParameterDirection.Input);
            parameters.Add("IsActive", entity.IsActive, DbType.Boolean, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryFirstOrDefaultAsync<Project>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
                var project = new Project()
                {
                    Name = entity.Name,
                    CreatedBy = createBy,
                    NumberMember = entity.NumberMember,
                    Icon = entity.Icon,
                    TimeExpiry = entity.TimeExpiry,
                    IsActive = entity.IsActive,
                };
                return project;
            }
        }

        public async System.Threading.Tasks.Task DeleteAsync(Guid id)
        {
            var procedureName = "spDeleteProject";
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Guid, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                await connection.QueryFirstOrDefaultAsync<Project>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Project>> GetAllAsync()
        {
            var procedureName = "EXECUTE spGetAllProject";
            
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryAsync<Project>(procedureName);
                return result.ToList();
            }
        }

        public async Task<Project> GetByIdAsync(Guid id)
        {
            var procedureName = "spGetByIdProject";
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Guid, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryFirstOrDefaultAsync<Project>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }

        public async Task<Project> UpdateAsync(UpdateProjectDto entity, Guid id)
        {
            var procedureName = "spUpdateProject";
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Guid, ParameterDirection.Input);
            parameters.Add("Name", entity.Name, DbType.String, ParameterDirection.Input);
            parameters.Add("NumberMember", entity.NumberMember, DbType.Int32, ParameterDirection.Input);
            parameters.Add("Icon", entity.Icon, DbType.String, ParameterDirection.Input);
            parameters.Add("TimeExpiry", entity.TimeExpiry, DbType.DateTime, ParameterDirection.Input);
            parameters.Add("IsActive", entity.IsActive, DbType.Boolean, ParameterDirection.Input);
            using (var connection = new SqlConnection(configuration.GetConnectionString("ConnectionString")))
            {
                var result = await connection.QueryFirstOrDefaultAsync<Project>
                    (procedureName, parameters, commandType: CommandType.StoredProcedure);
                var project = new Project()
                {
                    Id = id,
                    Name = entity.Name,
                    NumberMember = entity.NumberMember,
                    Icon = entity.Icon,
                    TimeExpiry = entity.TimeExpiry,
                    IsActive = entity.IsActive,
                };
                return project;
            }
        }
    }
}
