using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectManagerITUDA.Models.Authentication;
using ProjectManagerITUDA.Models.Dto;
using ProjectManagerITUDA.Models.Interface;

namespace ProjectManagerITUDA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InviteController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork unitOfWork;
        public InviteController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var data = await this.unitOfWork.Invites.GetAllAsync();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateInviteDto dto)
        {
            try
            {
                string tokenString = Request.Headers["Authorization"].ToString();
                var infoFromToken = Auths.GetInfoFromToken(tokenString);
                var userId = infoFromToken.Result.UserId;

                var data = await this.unitOfWork.Invites.AddAsync(dto, Guid.Parse(userId));
                return Ok(data);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(UpdateInviteDto dto, Guid id)
        {
            try
            {
                var check = await unitOfWork.Invites.GetByIdAsync(id);
                if (check == null)
                {
                    return NotFound(); //404 Not Found
                }
                var data = await unitOfWork.Invites.UpdateAsync(dto, id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var check = await unitOfWork.Invites.GetByIdAsync(id);
                if (check == null)
                {
                    return NotFound(); //404 Not Found
                }
                await unitOfWork.Invites.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetbyId(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound(); //404 Not Found
                }
                var data = await unitOfWork.Invites.GetByIdAsync(id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
