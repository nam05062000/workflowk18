﻿using ProjectManagerITUDA.Models.Authentication;
using ProjectManagerITUDA.Models.Interface;
using ProjectManagerITUDA.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace ProjectManagerITUDA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork unitOfWork;
        public UsersController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<object> Login([FromBody] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var checkLogin = await unitOfWork.Users.checkLogin(model.Username,model.Password);

                if (checkLogin == null)
                {
                    return BadRequest(new AuthResult()
                    {
                        Errors = new List<string>() {
                                "Invalid login request"
                            },
                        Success = false
                    });
                }
                else
                {
                    var jwtToken = GenerateJwtToken(checkLogin.Id);
                    return Ok(new AuthResult()
                    {
                        Success = true,
                        Token = jwtToken.Result
                    });
                }
            }

            return BadRequest();
        }


        [HttpGet]
        public async Task<IActionResult> GetProfile()
        {
            string tokenString = Request.Headers["Authorization"].ToString();
            var infoFromToken = Auths.GetInfoFromToken(tokenString);
            var userId = infoFromToken.Result.UserId;
            var data = await unitOfWork.Users.getProfile(Guid.Parse(userId));
            if (data == null) return BadRequest();
            return Ok(data);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public async Task<object> Register([FromBody] RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var checkRegister = await unitOfWork.Users.checkRegister(model.Username);

                if (checkRegister != null)
                {
                    return BadRequest(new AuthResult()
                    {
                        Errors = new List<string>() {
                                "Tài khoản đã tồn tại trong hệ thống"
                            },
                        Success = false
                    });
                }
                else
                {
                    await unitOfWork.Users.registerUser(model.Username, model.Password, model.Firstname, model.Lastname);
                    return Ok(new AuthResult()
                    {
                        Success = true,
                        Errors = new List<string>()
                        {
                            "Tạo tài khoản thành công!"
                        }
                    });
                }
            }

            return BadRequest();
        }
        

        private async Task<object> GenerateJwtToken(Guid id)
        {
            var claims = new List<Claim>
            {
                new Claim("id", id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddHours(Convert.ToDouble(_configuration["JwtExpireHours"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
