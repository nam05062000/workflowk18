﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectManagerITUDA.Models.Authentication;
using ProjectManagerITUDA.Models.Dto;
using ProjectManagerITUDA.Models.Interface;

namespace ProjectManagerITUDA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork unitOfWork;
        public CardController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var data = await this.unitOfWork.Cards.GetAllAsync();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateCardDto dto)
        {
            try
            {
                string tokenString = Request.Headers["Authorization"].ToString();
                var infoFromToken = Auths.GetInfoFromToken(tokenString);
                var userId = infoFromToken.Result.UserId;

                var data = await this.unitOfWork.Cards.AddAsync(dto );
                return Ok(data);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(UpdateCardDto dto, Guid id)
        {
            try
            {
                var check = await unitOfWork.Cards.GetByIdAsync(id);
                if (check == null)
                {
                    return NotFound(); //404 Not Found
                }
                var data = await unitOfWork.Cards.UpdateAsync(dto, id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var check = await unitOfWork.Cards.GetByIdAsync(id);
                if (check == null)
                {
                    return NotFound(); //404 Not Found
                }
                await unitOfWork.Cards.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetbyId(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound(); //404 Not Found
                }
                var data = await unitOfWork.Cards.GetByIdAsync(id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
